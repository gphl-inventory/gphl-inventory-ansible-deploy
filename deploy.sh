#!/bin/bash

# ANSIBLE VM DEPLOYMENT SCRIPT
#
# This script sets up a copy of Ansible on a fresh install of RHEL.  It is
# intended to be run as the root user.


PREFIX="/opt"
ANSIBLE_PREFIX="$PREFIX/ansible"
REPO_PREFIX="$PREFIX/gphl-inventory"

REPO_SERVER="https://gitlab.com/gphl-inventory/gphl-inventory-server.git"
REPO_APP="https://gitlab.com/gphl-inventory/gphl-inventory.git"

# Colors
COLOR_RED='\033[0;31m'
COLOR_GRN='\033[0;32m'
COLOR_NONE='\033[0m'

message_info () {
  echo -e "${COLOR_GRN}✔ $1${COLOR_NONE}"
}

message_error () {
  echo -e "${COLOR_RED}❌ $1${COLOR_NONE}"
  exit -1
}


NAME=$(basename $0)

usage () {
  cat << EOF
Usage: $NAME [OPTION]...

Deploy Ansible and checkout git repositories.

  -h, --help               Display this information.
  --prefix <path>          Install prefix.  Defaults to /opt
  --ansible-prefix <path>  Ansible prefix.  Defaults to \$PREFIX/ansible.
  --repo-prefix <path>     Repository prefix.  Defaults to \$PREFIX/gphl-inventory.
EOF
}

check_if_root_user () {
  message_info "Checking if root user ..."
  if [ "$EUID" -ne 0 ]; then
    message_error "Not root user.  Please run with root permissions."
  fi
}

check_if_rhel () {
  message_info "Checking if Red Hat Enterprise Linux ..."
  local os=$(cat /etc/os-release | grep -Po '^NAME="\K[^"]*')
  if [[ "$os" != "Red Hat Enterprise Linux" ]]; then
    message_error "Detected operating system $os is not Red Hat Enterprise Linux."
  fi
}

setup_packages () {
  message_info "Installing packages ..."
  yum update -y
  yum install -y git python3

  if [ $? -ne 0 ]; then 
    message_error "Failed to install required packages."
  fi
}

setup_ansible () {
  message_info "Installing Ansible ..."
  python3 -m venv $ANSIBLE_PREFIX
  local PIP="$ANSIBLE_PREFIX/bin/pip"
  $PIP install --upgrade pip

  # Check that pip upgrade exited without error
  if [ $? -ne 0 ]; then 
    message_error "Failed to upgrade pip."
  fi

  # Install ansible
  $PIP install ansible
  if [ $? -ne 0 ]; then 
    message_error "Failed to install ansible."
  fi

  # Update PATH for all users
  cat << END > /etc/profile.d/ansible.sh
# ansible initialization

export PATH="$PATH:$ANSIBLE_PREFIX/bin"
END
  source /etc/profile.d/ansible.sh

  message_info "Checking Ansible ..."
  ansible --version
  if [ $? -ne 0 ]; then
    message_error "Failed to install ansible."
  fi
}

# setup_server_repo () {
#   message_info "Checking out $REPO_SERVER ..."
#   mkdir -p $REPO_PREFIX
#   local here=$(pwd)
#   cd $REPO_PREFIX
# 
#   git clone $REPO_SERVER
#   if [ $? -ne 0 ]; then
#     cd $here
#     message_error "Failed to checkout repository."
#   fi
# 
#   cd $here
# }
 
setup_repo () {
  local REPO_NAME=$1
  local REPO_DIR=$2
  local REPO_URL=$3
  local here=$(pwd)

  if [ -d "$REPO_DIR" ]; then
    message_info "Repository $REPO_NAME already is checked out ..."
    cd "$REPO_DIR"
    git log -1
  else
    message_info "Checking out $REPO_NAME ..."
    mkdir -p $REPO_PREFIX
    cd $REPO_PREFIX

    git clone $REPO_URL
    if [ $? -ne 0 ]; then
      cd $here
      message_error "Failed to checkout repository."
    fi
  fi

  cd $here
}

setup_server_repo () {
  setup_repo $REPO_SERVER "$REPO_PREFIX/gphl-inventory-server" $REPO_SERVER
}

setup_application_repo () {
  setup_repo $REPO_APP "$REPO_PREFIX/gphl-inventory" $REPO_APP
}


install () {
  echo "✨ Installing Ansible ✨"
  echo ""
  
  check_if_root_user
  check_if_rhel
  setup_packages
  setup_ansible
  setup_server_repo
  setup_application_repo
 
  echo ""
  echo "Deployment completed ..."
  echo "🖖 Live long and Prosper!"
  echo ""
  echo "Start a new shell session or update the PATH in your current session"
  echo "by running \"source /etc/profile.d/ansible.sh\"."
}

# -- Main program --

SHOW_HELP=

ARGS=( "$@" )
for i in "${!ARGS[@]}"; do
  case "${ARGS[i]}" in
    "")
        continue
      ;;
    --prefix)
        PREFIX="${ARGS[i+1]}"
        ANSIBLE_PREFIX="$PREFIX/ansible"
        REPO_PREFIX="$PREFIX/gphl-inventory"
        unset 'ARGS[i+1]'
      ;;
    --ansible-prefix)
        ANSIBLE_PREFIX="${ARGS[i+1]}"
        unset 'ARGS[i+1]'
      ;;
    --repo-prefix)
        REPO_PREFIX="${ARGS[i+1]}"
        unset 'ARGS[i+1]'
      ;;
    -h | --help)
        SHOW_HELP=1
      ;;
    *)
        SHOW_HELP=1
        echo -e "${COLOR_RED}$NAME: invalid option -- '${ARGS[i]}'${COLOR_NONE}"
      ;;
  esac
  unset 'ARGS[i]'
done


if [ $SHOW_HELP ]; then
  usage
else
  install
fi
