# Ansible VM deployment script

Deployment script to setup ansible and git repositories on a fresh RHEL VM.

## Motivation

Ansible is a software tool that enables infrastructure as code.  We use ansible
to automate application deployment on our infrastructure.  Generally, it's good
practice to run ansible on a separate machine than the deployment targets.

While it's straightforward to install ansible locally, we've opted to use a
separate VM with the ansible toolchain installed.  This allows system
administrators to have access to a common ansible environment, and reduces the
overhead of setting up ansible on each administrator's machine.

This script should be used if we have to redeploy ansible to a fresh VM.

## Usage

The deployment is managed by a bash script that should be executed as the root
user.  First, open a shell session on the remote VM and login as root

```bash
sudo -i
```

Next, download the bash script into your local directory

```bash
curl https://gitlab.com/gphl-inventory/gphl-inventory-ansible-deploy/-/raw/main/deploy.sh > $HOME/deploy.sh 
```

Check the md5 sum of the script
```bash
md5sum $HOME/deploy.sh
```

```
5ded013356b528fa5122892717aef36d  /root/deploy.sh
```

If the md5 sum matches, we are safe to execute the script.  Go ahead and run

```bash
bash $HOME/deploy.sh
```

You can also view other configuration options by passing the `--help` flag
```bash
bash $HOME/deploy.sh --help
```

```
Usage: deploy.sh [OPTION]...

Deploy Ansible and checkout git repositories.

  -h, --help               Display this information.
  --prefix <path>          Install prefix.  Defaults to /opt
  --ansible-prefix <path>  Ansible prefix.  Defaults to $PREFIX/ansible.
  --repo-prefix <path>     Repository prefix.  Defaults to $PREFIX/gphl-inventory.
```

The script will install and configure ansible.  Near the end, it will
prompt you for your gitlab username and password.  This should be the username
and password a gitlab account which has been grated access to our (private)
code repositories.

As the script finishes, you will see the following message:

```
Deployment completed ...
🖖 Live long and Prosper!

Start a new shell session or update the PATH in your current session
by running "source /etc/profile.d/ansible.sh".
```

Go ahead and start a new shell session.  Ansible should now be installed on
your PATH.
